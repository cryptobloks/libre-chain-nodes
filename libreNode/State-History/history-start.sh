#!/bin/bash
################################################################################
source /opt/libre-chain-nodes/libreNode/node.env

$MAINDIR/nodeos --data-dir $DATADIR --config-dir $MAINDIR --genesis-json $MAINDIR/genesis.json  --disable-replay-opts "$@"  2> $DATADIR/stderr.txt &  echo $! > $DATADIR/nodeos.pid
